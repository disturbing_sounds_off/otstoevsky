package com.example.otstoevsky.ui

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.otstoevsky.ui.screens.*

enum class Screens {
    Cart, Chat, Home, Menu, Profile
}

@Composable
fun OtstoevskyApp() {

    val viewModel: OrderViewModel = viewModel()

    val navController = rememberNavController()

    val currentScreenName = Screens.valueOf(
        navController.currentBackStackEntryAsState().value?.destination?.route ?: Screens.Home.name
    )

    Scaffold (

            ){ padding ->
        val orderState = viewModel.uiState.collectAsState()

        NavHost(
            modifier = Modifier.padding(padding),
            navController = navController,
            startDestination = Screens.Home.name
        ) {

            composable(route = Screens.Home.name) {
                HomeScreen(onNextButtonPress = {
                    navController.navigate(Screens.Menu.name)
                })
            }

            composable(route = Screens.Menu.name) {
                MenuScreen(onNextButtonPress = {
                    navController.navigate(Screens.Cart.name)
                })
            }

            composable(route = Screens.Cart.name) {
                CartScreen(onNextButtonPress = {
                    navController.navigate(Screens.Chat.name)
                })
            }

            composable(route = Screens.Chat.name) {
                ChatScreen(onNextButtonPress = {
                    navController.navigate(Screens.Profile.name)
                })
            }

            composable(route = Screens.Profile.name) {
                ProfileScreen(onNextButtonPress = {
                    navController.navigate(Screens.Home.name)
                })
            }
        }
    }
}
