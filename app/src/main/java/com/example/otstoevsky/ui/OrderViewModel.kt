package com.example.otstoevsky.ui

import androidx.lifecycle.ViewModel
import com.example.otstoevsky.model.OrderState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class OrderViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(OrderState(emptyList()))
    val uiState = _uiState.asStateFlow()

}