package com.example.otstoevsky.model


data class OrderState(
    val orders: List<Order>
)
