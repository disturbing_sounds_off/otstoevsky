package com.example.otstoevsky.model

import androidx.annotation.StringRes

data class Order(
    val id: Int,
    @StringRes val img: Int,
    val count: Int
)
